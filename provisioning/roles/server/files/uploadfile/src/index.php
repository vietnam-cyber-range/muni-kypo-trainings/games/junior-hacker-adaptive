<?php
// Tạo thư mục cho mỗi người dùng
session_start();
if (!isset($_SESSION['dir'])) {
    $_SESSION['dir'] = 'upload/' . session_id();
}
$dir = $_SESSION['dir'];
if (!file_exists($dir))
    mkdir($dir);

if (isset($_GET["debug"])) die(highlight_file(__FILE__));

if (isset($_FILES["file"])) {
    $error = '';
    $success = '';
    try {
        $file = $dir . "/" . $_FILES["file"]["name"];
        move_uploaded_file($_FILES["file"]["tmp_name"], $file);
        $success = 'Successfully uploaded file at: <a href="/' . $file . '">/' . $file . ' </a><br>';
        $success .= 'View all uploaded file at: <a href="/' . $dir . '/">/' . $dir . ' </a>';
    } catch (Exception $e) {
        $error = $e->getMessage();
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>File Upload Vulnerabilities</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css" integrity="sha512-P5MgMn1jBN01asBgU0z60Qk4QxiXo86+wlFahKrsQf37c9cro517WzVSPPV1tDKzhku2iJ2FVgL67wG03SGnNA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- FontAwesome for Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-PLBFi4k2q/qB0IAYZ5s5LpjZaAFCQ8eE69C6HfyfPU6LRchhvK2RFeMvj50C1bG8Qh6nU4nTR2E0kgRgO4B2PQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/static/css/style.css">

</head>

<body >

    <div class="wrapper">
        <div class="container-custom">
            <h2>File Upload Vulnerabilities </h2>
            <h4>Level 1</h4>
            <p>Flag format: VNCERT/CC{dUJTEm7aLPP...}!</p>

            <div class="upload-container">
                <div class="border-container" id="drop-area">
                    <div class="icons fa-4x">
                        <i class="fas fa-file-image" data-fa-transform="shrink-3 down-2 left-6 rotate--45"></i>
                        <i class="fas fa-file-alt" data-fa-transform="shrink-2 up-4"></i>
                        <i class="fas fa-file-pdf" data-fa-transform="shrink-3 down-2 right-6 rotate-45"></i>
                    </div>
                    <p>Drag and drop files here, or <a href="#" id="file-browser">browse</a> your computer.</p>
                    <form method="post" enctype="multipart/form-data" id="file-form">
                        <input type="file" name="file" id="file-upload" class="d-none">
                        <br />
                        <button type="submit" class="btn btn-primary mt-3">Upload</button>
                    </form>
                    <div class="msg">
                        <span class="text-danger"><?php echo $error; ?></span>
                        <span class="text-success"><?php echo $success; ?></span>
                    </div>
                </div>
            </div>
            <a href="/?debug" class="btn btn-info mt-3">Debug Source</a>
        </div>
    </div>

    <footer class="container mt-5">
        <div class="row justify-content-between">
            <div class="col-md-2">
                <button class="btn btn-dark" type="button" onclick="prevLevel()">Previous Level</button>
            </div>
            <div class="col-md-2 text-right">
                <button class="btn btn-dark" type="button" onclick="nextLevel()">Next Level</button>
            </div>
        </div>
    </footer>

    <script>
        function prevLevel() {
            const url = new URL(window.location.origin);
            url.port = (parseInt(url.port) - 1).toString();
            location.href = url.toString();
        }

        function nextLevel() {
            const url = new URL(window.location.origin);
            url.port = (parseInt(url.port) + 1).toString();
            location.href = url.toString();
        }

        document.getElementById('file-browser').addEventListener('click', function(e) {
            e.preventDefault();
            document.getElementById('file-upload').click();
        });

        const dropArea = document.getElementById('drop-area');

        ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
            dropArea.addEventListener(eventName, preventDefaults, false)
        });

        function preventDefaults(e) {
            e.preventDefault();
            e.stopPropagation();
        }

        dropArea.addEventListener('dragover', () => {
            dropArea.classList.add('dragover');
        });

        dropArea.addEventListener('dragleave', () => {
            dropArea.classList.remove('dragover');
        });

        dropArea.addEventListener('drop', handleDrop, false);

        function handleDrop(e) {
            const dt = e.dataTransfer;
            const files = dt.files;
            handleFiles(files);
        }

        function handleFiles(files) {
            const fileInput = document.getElementById('file-upload');
            const dataTransfer = new DataTransfer();
            dataTransfer.items.add(files[0]);
            fileInput.files = dataTransfer.files;
        }
    </script>

    <!-- Bootstrap JS and dependencies -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.slim.min.js" integrity="sha512-5AW1pkeGAW3/D8P+vU23OY79Z9v6K2T4Qp7T5Nf3ipm5HGmle8VzzOK7p5v5kgh49/cwN+9G3hP5x0DjAgfP1w==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js" integrity="sha512-kV0YtxeZclceFciSTFbgnys5JLPfpmOBoVf/EUlVsIIB9fdYkBY6hrB08daYhRPp+bfY1erLh+PzJGDvUu8jlQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js" integrity="sha512-mRxBL7C+jNkgjG+xp+Ks06JEnl1g7RqpiQ/sGYPnMj0l1WxlcCCM6RaCkYrXsJk7SzlQLgjZFY8mEy1lMi/c1A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</body>

</html>
